
import * as common from '../common/common.js';

var atlas = {
    "ac24.cz": "AC24",
    "aeronet.cz": "Aeronet",
    "arfa.cz": "Arfa",
    "bezpolitickekorektnosti.cz": "Bez_politické_korektnosti",
    "casopis-sifra.cz": "Časopis_Šifra",
    "ceskoaktualne.cz": "Česko_aktuálně",
    "czechfreepress.cz": "Czech_Free_Press",
    "cz24.news": "CZ_24_News",
    "eportal.cz": "EPortál",
    "euportal.cz": "EUportál",
    "freeglobe.cz": "FreeGlobe",
    "naseveru.org": "Na_severu",
    "novarepublika.cz": "Nová_republika",
    "nwoo.org": "New_World_Order_Opposition",
    "otevrisvoumysl.cz": "Otevři_svou_mysl",
    "outsidermedia.cz": "Outsider_Media",
    "pravdive.eu": "Pravdivě",
    "protiproud.info": "Protiproud",
    "pravyprostor.net": "Pravý_prostor",
    "prvnizpravy.cz": "První_zprávy",
    "rizikavakcin.org": "Rizika_vakcín",
    "rukojmi.cz": "Rukojmí",
    "skrytapravda.cz": "Skrytá_pravda",
    "stredoevropan.cz": "Středoevropan",
    "svobodnenoviny.eu": "Svobodné_noviny",
    "tydenikobcanskepravo.cz": "TOP_News",
    "tadesco.org": "Tadesco",
    "veksvetla.cz": "Věk_světla",
    "vlasteneckenoviny.cz": "Vlastenecké_noviny",
    "zvedavec.org": "Zvědavec"
};

var contentElement = document.getElementById("content");

chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    var tab = tabs[0];
    if (tab.url && (tab.url.startsWith('http://') || tab.url.startsWith('https://'))) {
        const hostURL = new URL(tab.url).host.replace('www.','');
        contentElement.innerHTML = 'Stránka nebyla rozpoznána jako nedůvěryhodná.';
        if (common.isCzechoslovakURLBad(hostURL)) {
            contentElement.innerHTML = 'Stránka byla nalezena v seznamu:\n<a href="https://www.konspiratori.sk/zoznam-stranok" target="_blank">Konšpirátori.sk</a>\n';
            if (hostURL in atlas) {
                contentElement.innerHTML += `<h3>Více informací o stránce:</h3>\n<a href="https://atlaskonspiraci.cz/${atlas[hostURL]}" target="_blank">Atlas konspirací</a>`;
            }
        } else {
            fetch(`https://api.newsguardtech.com/check/${hostURL}`)
            .then(function(response) {
                return response.json();
            }).then(function(responseJson) {
                if (responseJson.rank == 'N') {
                    contentElement.innerHTML = 'Stránka byla nalezena v seznamu:\n<a href="https://www.newsguardtech.com/" target="_blank">NewsGuard</a>\n';
                }
            });
        }
    }
});
