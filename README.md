# Štít proti hoaxům

Tento doplněk využívá veřejně dostupný seznam českých a slovenských webů se sporným obsahem. (<https://www.konspiratori.sk/zoznam-stranok>)
Při návštěvě webu ověří, zda se tento web nenachází na tomto seznamu a v případě, že ano, změní svou ikonu na výstražnou.
Pokud navštívený web v tomto seznamu nenajde, dotáže se na něj API <https://www.newsguardtech.com/> a ikonku změní na výstražnou, pokud je tímto API web vyhodnocen jako nedůvěryhodný.
