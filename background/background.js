
import * as common from '../common/common.js';

function changeIconToRed(tabId) {
    const icon = '../icons/red.png';
    chrome.browserAction.setIcon({
        'path': {
            128: icon
        },
        'tabId': tabId
    });
}

chrome.tabs.onUpdated.addListener(async (id, info, tab) => {
    if (info.url && (tab.url.startsWith('http://') || tab.url.startsWith('https://'))) {
        const hostURL = new URL(tab.url).host;
        if (common.isCzechoslovakURLBad(hostURL)) {
            changeIconToRed(id);
        } else {
            fetch(`https://api.newsguardtech.com/check/${hostURL}`)
                .then(function(response) {
                    return response.json();
                }).then(function(responseJson) {
                    if (responseJson.rank == 'N') {
                        changeIconToRed(id);
                    }
                });
        }
    }
});
